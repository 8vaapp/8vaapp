import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  // state basicamente almacena las variables globales a las que puede acceder cualquier componente
  state:
  {
    isMobile: false
  },
  //Mutations es como se llaman los metodos o funciones en el patron VUEX
  mutations:
  {
    checkMobile: function(state)
    {
      let str = navigator.userAgent.toLowerCase();

      let android = str.search("android");
      let ios = str.search("iphone");

      if(android != -1 || ios != -1)
      {
        //this.isMobile = true
        state.isMobile = true;
        console.log(state.isMobile);
      }

      console.log(str);

    }
  }

})