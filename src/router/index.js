import Vue from 'vue'
import Router from 'vue-router'
import main from '@/components/main.vue'
import menuInferior from '@/components/menuInferior.vue'
import login from '@/components/login.vue'
import youtube from '@/components/youtube.vue'
import youtube2 from '@/components/youtube2.vue'
import habitos from '@/components/habitos.vue'
import detallesHabitos from '@/components/detallesHabitos.vue'
import nuevaTarea from '@/components/nuevaTarea.vue'
import detallesTarea from '@/components/detallesTarea.vue'
import tienda from '@/components/tienda.vue'
import temporizador from '@/components/temporizador.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      components: 
      {
        default: main,
        a: menuInferior
      }
    },
    {
      path: '/login',
      components: 
      {
        default: login
      }
    },
    {
      path: '/youtube',
      components: 
      {
        default: youtube
      }
    },
    {
      path: '/youtube2',
      components: 
      {
        default: youtube2
      }
    },
    {
      path: '/habitos',
      components: 
      {
        default: habitos,
        c: menuInferior
      }
    },
    {
      path: '/detallesHabitos',
      components: 
      {
        default: detallesHabitos,
        c: menuInferior
      }
    },
    {
      path: '/nuevaTarea',
      components: 
      {
        default: nuevaTarea
      }
    },
    {
      path: '/detallesTarea',
      components: 
      {
        default: detallesTarea
      }
    },
    {
      path: '/tienda',
      components: 
      {
        default: tienda
      }
    },
    {
      path: '/temporizador',
      components: 
      {
        default: temporizador
      }
    }
  ]
})
